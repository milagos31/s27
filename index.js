const http = require("http");
const port = 3000;

//Mock database
const directory = [
    { name: "Ike", email: "" },
    { name: "Manuel", email: "" },
];

const server = http.createServer((req, res) => {
    console.log("Client requesting URL:", req.url);
    // GETTING users
    if (req.url == "/users" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });

        res.write(JSON.stringify(directory));
        res.end();
    } // POSTING users
    else if (req.url == "/users" && req.method == "POST") {
        //   res.writeHead(200, {});
        //   res.end("New user sent to the Backend");
        let requestBody = "";

        req.on("data", (data) => {
            console.log("data:", data);
            requestBody += data;
        });

        req.on("end", () => {
            console.log(requestBody);

            requestBody = JSON.parse(requestBody);

            console.log(requestBody);

            let newUser = { name: requestBody.name, email: requestBody.email };

            directory.push(newUser);
            console.table(directory);

            res.writeHead(200, { "Content-Type": "application/json" });
            res.write(JSON.stringify(newUser));
            res.end();
        });
    }
});

server.listen(port, () => {
    console.log("Listening to port: ", port);
});
